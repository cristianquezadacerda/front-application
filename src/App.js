import React, { PureComponent } from "react";
import './App.css';
import Mascotas from './Mascotas';
import filterMascotas from './filterMascotas';

export default class App extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      filteredMascotas: filterMascotas("", 20)
    };
  }

  render() {

    return (
      <div>
        <Mascotas mascotasData={this.state.filteredMascotas} />
      </div>
    );
  }
}
