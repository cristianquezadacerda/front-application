import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import MascotaRow from "./MascotaRow";
import "./Mascotas.css";
import { MdAdd } from "react-icons/md";
import Button from 'react-bootstrap/Button';

export default class Mascotas extends PureComponent {
    static propTypes = {
        mascotasData: PropTypes.array
      };

    render() {
        return (
            <table className="component-mascota-results">
                <tbody>
                    <tr>
                        <td colSpan={6}></td>
                        <td><Button><MdAdd /> Agregar</Button></td>
                    </tr>
            {this.props.mascotasData.map(mascotasData => (
              <MascotaRow
                idmascota={mascotasData['id-mascota']}
                nombre={mascotasData.nombre}
                descripcion={mascotasData.descripcion}
                edad={mascotasData.edad}
                familia={mascotasData.familia}
                info={
                    mascotasData.familia.toUpperCase() === "PERRO"
                    ? mascotasData.tipo
                    : mascotasData.raza
                }
              />
            ))}
                </tbody>
            </table>
        );
    }
}