import mascotasList from "./mascotasList.json";

export default function filterMascotas(searchText, maxResults) {
  return mascotasList
    .filter(mascota => {
      if (mascota.nombre.toLowerCase().includes(searchText.toLowerCase())) {
        return true;
      }
      if (mascota.descipcion.toLowerCase().includes(searchText.toLowerCase())) {
        return true;
      }
      return false;
    })
    .slice(0, maxResults);
}