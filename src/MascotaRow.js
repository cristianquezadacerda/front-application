import Button from 'react-bootstrap/Button';
import { MdEdit, MdDelete } from "react-icons/md";
import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import "./MascotaRow.css";

export default class MasctoraRow extends PureComponent {

    static propTypes = {
        nombre: PropTypes.string,
        descripcion: PropTypes.string,
        edad: PropTypes.number,
        familia: PropTypes.string,
        info: PropTypes.string
      };
    
    render () {
        return (
            <tr className="component-mascota-row">
              <td className="idmascota">{this.props.idmascota}</td>
              <td className="nombre">{this.props.nombre}</td>
              <td className="descripcion">{this.props.descripcion}</td>
              <td className="edad">{this.props.edad}</td>
              <td className="familia">{this.props.familia}</td>
              <td className="info">{this.props.info}</td>
              <td className="tools">
                <Button><MdEdit /></Button>
                <Button><MdDelete /></Button>
              </td>
            </tr>
        );
    }
}